import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;

/**
 * Created by sbunce on 5/30/2016.
 */
public class MainButtonsBar extends HBox{
    private HBox buttonBar = new HBox();

    public MainButtonsBar(){
        super();
        this.initialize();
    }

    public void initialize(){
        buttonBar.setSpacing(10);
        buttonBar.setPadding(new Insets(15, 12, 15, 12));
        buttonBar.setStyle("-fx-background-color: #DCDCDC;");

        Button button1 = new Button("Create New Table");
        button1.setPrefSize(150, 30);

        Button button2 = new Button("Save Table");
        button2.setPrefSize(150, 30);

        Button button3 = new Button("Clear Table");
        button3.setPrefSize(150, 30);

        Button button4 = new Button("Generate Plots");
        button4.setPrefSize(150, 30);

        buttonBar.getChildren().addAll(button1, button2, button3, button4);
    }

    //Returns compatible type to be added to main window
    public HBox getButtons(){
        return buttonBar;
    }
}
